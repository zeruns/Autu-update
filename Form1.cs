using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace 自动更新程序
{

    public partial class Form1 : Form
    {
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileInt(
         string lpAppName,// 指向包含 Section 名称的字符串地址
              string lpKeyName,// 指向包含 Key 名称的字符串地址
              int nDefault,// 如果 Key 值没有找到，则返回缺省的值是多少
              string lpFileName);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(
          string lpAppName,// 指向包含 Section 名称的字符串地址
      string lpKeyName,// 指向包含 Key 名称的字符串地址
      string lpDefault,// 如果 Key 值没有找到，则返回缺省的字符串的地址
      StringBuilder lpReturnedString,// 返回字符串的缓冲区地址
      int nSize,// 缓冲区的长度
      string lpFileName);
        [DllImport("kernel32")]
        private static extern bool WritePrivateProfileString(
         string lpAppName,// 指向包含 Section 名称的字符串地址
              string lpKeyName,// 指向包含 Key 名称的字符串地址
              string lpString,// 要写的字符串地址
              string lpFileName
         );

        public Form1()
        {
            InitializeComponent();
        }


        private void 自动更新程序_Load(object sender, EventArgs e)
        {
            progressBar1.Value = 0;

            label1.Text = "";

            label3.Text = "";

        }




        private void button2_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.mcmxzl.com");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string path = System.IO.Directory.GetCurrentDirectory();//获取当前运行目录
            string FilePath = path + "/判断闰年.exe";//需要更新的程序
            System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(FilePath);//获取文件版本信息
            label3.Text = "正在获取更新信息";
            try
            {
                DownloadFile2("http://7xsjsl.com2.z0.glb.clouddn.com/upconfig.ini", path + "/upconfig.ini", progressBar1, label1);//获取最新版本
                WritePrivateProfileString("版本", "当前版本", fvi.FileVersion, path + "/upconfig.ini");//写入当前版本信息到配置文件
            }
            catch
            {
                label3.Text = label3.Text + "\n更新信息获取失败，请检查您的网络。";
            }
            
            System.Text.StringBuilder temp = new System.Text.StringBuilder(255);//读配置时用到的变量
            GetPrivateProfileString("版本", "最新版本", "",temp, 255, path + "/upconfig.ini");
            string banben=Convert.ToString(temp);
            Version zbanben = new Version(banben);
            Version xbanben = new Version(fvi.FileVersion);
            if ( zbanben > xbanben)
            {
                try
                {
                    DownloadFile("http://7xsjsl.com2.z0.glb.clouddn.com/判断闰年.exe", path + "/判断闰年.exe", progressBar1, label1);//下载新版本
                    label3.Text = label3.Text + "\n更新成功。";
                    fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(FilePath);//获取文件版本信息
                    WritePrivateProfileString("版本", "当前版本", fvi.FileVersion, path + "/upconfig.ini");//写入当前版本信息到配置文件
                    
                }
                catch
                {
                    label3.Text = label3.Text + "\n下载失败，请检查您的网络。";
                }

            }
            else
            {
                label3.Text = label3.Text + "\n当前版本为最新版本。";
            }
        }

        public void DownloadFile(string URL, string filename, System.Windows.Forms.ProgressBar prog, System.Windows.Forms.Label label1)
        {
            float percent = 0;

            System.Net.HttpWebRequest Myrq = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(URL);
            System.Net.HttpWebResponse myrp = (System.Net.HttpWebResponse)Myrq.GetResponse();
            long totalBytes = myrp.ContentLength;
            if (prog != null)
            {
                prog.Maximum = (int)totalBytes;
            }
            System.IO.Stream st = myrp.GetResponseStream();
            System.IO.Stream so = new System.IO.FileStream(filename, System.IO.FileMode.Create);
            long totalDownloadedByte = 0;
            byte[] by = new byte[1024];
            int osize = st.Read(by, 0, (int)by.Length);
            while (osize > 0)
            {
                totalDownloadedByte = osize + totalDownloadedByte;
                System.Windows.Forms.Application.DoEvents();
                so.Write(by, 0, osize);
                if (prog != null)
                {
                    prog.Value = (int)totalDownloadedByte;
                }
                osize = st.Read(by, 0, (int)by.Length);

                percent = (float)totalDownloadedByte / (float)totalBytes * 100;
                label1.Text = "当前文件下载进度" + percent.ToString() + "%";
                System.Windows.Forms.Application.DoEvents(); //必须加注这句代码，否则label1将因为循环执行太快而来不及显示信息
            }
            so.Close();
            st.Close();



        }
        public void DownloadFile2(string URL, string filename, System.Windows.Forms.ProgressBar prog, System.Windows.Forms.Label label1)
        {


            System.Net.HttpWebRequest Myrq = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(URL);
            System.Net.HttpWebResponse myrp = (System.Net.HttpWebResponse)Myrq.GetResponse();
            long totalBytes = myrp.ContentLength;
            if (prog != null)
            {
                prog.Maximum = (int)totalBytes;
            }
            System.IO.Stream st = myrp.GetResponseStream();
            System.IO.Stream so = new System.IO.FileStream(filename, System.IO.FileMode.Create);
            long totalDownloadedByte = 0;
            byte[] by = new byte[1024];
            int osize = st.Read(by, 0, (int)by.Length);
            while (osize > 0)
            {
                totalDownloadedByte = osize + totalDownloadedByte;
                System.Windows.Forms.Application.DoEvents();
                so.Write(by, 0, osize);
                if (prog != null)
                {

                }
                osize = st.Read(by, 0, (int)by.Length);



                System.Windows.Forms.Application.DoEvents(); //必须加注这句代码，否则label1将因为循环执行太快而来不及显示信息
            }
            so.Close();
            st.Close();

        }
    }
}
